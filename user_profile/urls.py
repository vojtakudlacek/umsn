from django.urls import include, path
from . import views

urlpatterns = [
    path('register/', views.register, name='register'),
    path('account/profile', views.profile, name='user_profile'),
]
