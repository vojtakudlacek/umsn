from django.shortcuts import render, redirect
from .forms import RegistrationForm
from social_network.models import Board

# Create your views here.


def register(req):
    if req.method == 'POST':
        form = RegistrationForm(req.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = RegistrationForm()
    return render(req, 'user_profile/register.html', {'form': form})


def profile(req):
    created_boards = Board.objects.filter(owner=req.user)
    if req.method == 'POST':
        if req.POST['board'].startswith('d'):
            board = Board.objects.get(id=req.POST['board'][1:])
            if board.owner == req.user:
                board.delete()
            else:
                redirect('/login')
        else:
            b = Board.objects.filter(id=int(req.POST['board']))
            req.user.board_set.remove(b[0])
    boards = req.user.board_set.all()[::-1]
    return render(req, 'user_profile/profile.html', {'boards': boards, 'created_boards': created_boards})
