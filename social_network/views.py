from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from .models import Board, FriendRequest, Friends, Post
from .forms import PostForm, CreateBoardForm

# Create your views here.


def index(req, page=1):
    if not req.user.is_authenticated:
        posts = Post.objects.all()[::-1]
    else:
        userBoards = req.user.board_set.all()
        posts = []
        for board in userBoards:
            posts_in_board = Post.objects.filter(board=board)[::-1]
            posts += posts_in_board
        posts = sorted(posts, key=lambda x: x.id, reverse=True)

    listOfPages = [i+1 for i in range(len(posts) // 5 + 1)]
    if len(posts) % 5 == 0:
        listOfPages.pop()

    posts = posts[(page-1)*5:page*5]
    return render(req, 'social_network/index.html', {'posts': posts, 'page': str(page), 'listOfPages': listOfPages})


def board(req, ids=1, page=1):
    board = Board.objects.filter(id=ids)[0]
    if req.method == 'POST':
        form = PostForm(req.POST)

        if form.is_valid() and req.user.is_authenticated:
            new_post = Post(
                poster=req.user, content=form.cleaned_data['post_content'], board=board)
            new_post.save()
    else:
        form = PostForm()

    posts = Post.objects.filter(board=board)[::-1]
    posts = posts[(page-1)*5:page*5]

    interested = board.interested.all()

    listOfPages = [i+1 for i in range(len(posts) // 5 + 1)]

    return render(req, 'social_network/board.html', {'posts': posts, 'board': board, 'form': form, 'interested': interested, 'page': page, 'listOfPages': listOfPages})


def interest(req, ids=1):
    board = Board.objects.filter(id=ids)[0]
    if req.user.is_authenticated:
        board.interested.add(req.user)
    return redirect('/b/' + str(board.id))


def uninterest(req, ids=1):
    board = Board.objects.filter(id=ids)[0]
    if req.user.is_authenticated:
        board.interested.remove(req.user)
    return redirect('/b/' + str(board.id))


def boards(req):
    if req.method == 'POST':
        form = CreateBoardForm(req.POST)
        if form.is_valid() and req.user.is_authenticated:
            new_board = Board(name=form.cleaned_data['board_name'],
                              description=form.cleaned_data['board_description'], owner=req.user)
            new_board.save()
            return redirect('/b/' + str(new_board.id))
    else:
        form = CreateBoardForm()

    boards = Board.objects.all()
    return render(req, 'social_network/search.html', {'boards': boards, 'b': True, 'form': form})


def search(req):
    if req.method == "GET":
        if not req.GET.__contains__("q"):
            return redirect("/b")
        query = req.GET.get("q")
        if query == "":
            return redirect("/b")
        boards = Board.objects.filter(name__startswith=query)

        return render(req, 'social_network/search.html', {'boards': boards, 'query': query, 'b': False})
    else:
        return redirect("/b")
