from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Board(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1024)
    interested = models.ManyToManyField(User)
    owner = models.ForeignKey(
        User, related_name='board_owner', on_delete=models.DO_NOTHING, null=True)


class Friends(models.Model):
    user1 = models.ManyToManyField(User)
    current_user = models.ForeignKey(
        User, related_name='frieds_owner', on_delete=models.CASCADE, null=True)

    @classmethod
    def make_friend(cls, current_user, new_friend):
        friend, create = cls.objects.get_or_create(
            current_user=current_user
        )
        friend.users1.add(new_friend)

    @classmethod
    def lose_friend(cls, current_user, new_friend):
        friend, create = cls.objects.get_or_create(
            current_user=current_user
        )
        friend.users1.remove(new_friend)


class FriendRequest(models.Model):
    sender = models.ForeignKey(
        User, null=True, related_name='sender1', on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, null=True, on_delete=models.CASCADE)


class Post(models.Model):
    poster = models.ForeignKey(
        User, related_name='poster', on_delete=models.CASCADE, null=False)
    content = models.CharField(max_length=2048)
    board = models.ForeignKey(
        Board, on_delete=models.CASCADE, related_name='board', null=False)
