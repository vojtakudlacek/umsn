from django.contrib import admin
from .models import Post, Board, FriendRequest, Friends

# Register your models here.
admin.site.register(Post)
admin.site.register(Board)
admin.site.register(FriendRequest)
admin.site.register(Friends)
