from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('<int:page>', views.index),
    path('b/<int:ids>', views.board),
    path('b/<int:ids>/<int:page>', views.board),
    path('b/<int:ids>/interest', views.interest),
    path('b/<int:ids>/uninterest', views.uninterest),
    path('b/', views.boards),
    path('search', views.search),
]
