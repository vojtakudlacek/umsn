from django import forms


class PostForm(forms.Form):
    post_content = forms.CharField(
        label="Enter you post here ", max_length=2048, widget=forms.Textarea())


class CreateBoardForm(forms.Form):
    board_name = forms.CharField(
        label="Enter board name here ", max_length=200)
    board_description = forms.CharField(
        label="Enter you board description here ", max_length=1024, widget=forms.Textarea())
